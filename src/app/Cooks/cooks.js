/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take care of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'myrecipes.cooks', [
  'myrecipes.services',
  'ui.router',
  'firebase'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider
  .state( 'cooks', {
    url: '/cooks',
    views: {
      "main": {
        controller: 'CooksCtrl',
        templateUrl: 'cooks/cooks.tpl.html'
      }
    },
    data:{ pageTitle: 'Cooks' }
  })

  .state( 'cooks.feed', {
    url: '/:id',
    parent: 'cooks',
    views: {
      "content": {
        controller: 'CooksFeedCtrl',
        templateUrl: 'cooks/cooksFeed.tpl.html'
      }
    },
    data:{ pageTitle: 'Cooks Feed' }
  })

  .state( 'cooks.recipe', {
    url: '/:id/recipe/:recid',
    parent: 'cooks',
    views: {
      "content": {
        controller: 'CooksRecipeCtrl',
        templateUrl: 'cooks/cooksRecipe.tpl.html'
      }
    },
    data:{ pageTitle: 'Cooks Recipe' }
  })
  ;
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'CooksCtrl', function CooksController( $scope, $rootScope, $firebase, firebaseRef ) {
  $scope.cooks = $firebase(firebaseRef('users'));
  // console.log($scope.cooks);
  $scope.size = function(d){
    return _.size(d);
  };
})

.controller( 'CooksFeedCtrl', function CooksFeedController( $scope, $stateParams, $firebase, firebaseRef ) {
  
  $scope.cookid = $stateParams.id;
  $scope.cookname = $firebase(firebaseRef(['users',$scope.cookid]));
  // console.log( $scope.cookname );

  $scope.cookrecipelist = $scope.cookname.$child('recipe');
  // console.log($scope.cookrecipelist);
  $scope.getDate = function(d){
    var myDate = new Date(d);
    return myDate.toDateString();
  };
})


.controller( 'CooksRecipeCtrl', function CooksRecipeController( $scope, $stateParams, $firebase, firebaseRef ) {
  
  $scope.cookid = $stateParams.id;
  $scope.cookrecipe = $firebase(firebaseRef(['users',$scope.cookid,'recipe',$stateParams.recid]));
  // console.log($scope.cookrecipe);
})

;

