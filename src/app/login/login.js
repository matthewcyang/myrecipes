/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/login`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'myrecipes.login', [
  'myrecipes.services',
  'ui.router',
  'firebase'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider
  .state( 'login', {
    url: '/login',
    views: {
      "main": {
        controller: 'LoginCtrl',
        templateUrl: 'login/login.tpl.html'
      }
    },
    data:{ pageTitle: 'Login' }
  });
})

.run(['loginService', '$rootScope', 'FBURL', function(loginService, $rootScope, FBURL){
  
  $rootScope.auth = loginService.init('/login');
  $rootScope.FBURL = FBURL;

}])

/**
 * And of course we define a controller for our route.
 */
.controller( 'LoginCtrl', function LoginController($location, loginService, $scope) {
  // $scope.myId = $stateParams.myId;
 
  // monitor state changes and react to updates
  // var authClient = new FirebaseSimpleLogin(firebase, function(error, user) {
  //     if (error) {
  //         // an error occurred while attempting login
  //         console.log(error);
  //     } else if (user) {
  //         // user authenticated with Firebase
  //         console.log('User ID: ' + user.id + ', Provider: ' + user.provider);
  //     } else {
  //         // user is logged out
  //         console.log('User logged out');
  //     }
  // });

  
  // $scope.$storage = $localStorage;
  $scope.isLoggedIn = loginService.isLoggedIn;

  $scope.$on('$firebaseAuth:login', function() {  
     $location.path('/main/listing').replace();
  });

  // if($scope.isLoggedIn()){
  //   $state.go('main.listing');
  // }
  // $scope.doLogin = function(){ authService.logIn($scope.user.login, $scope.user.password); };
  // $scope.doLogout = authService.logOut;
  // console.log($rootScope.auth);
  $scope.doFBLogin = function(){ loginService.login('facebook'); };
  // $scope.doFBLogin = function(){$rootScope.auth.$login('facebook');};
  // $scope.doFBLogin =  authService.logIn;
  $scope.doFBLogout =  loginService.logout;
})


// .constant('FBURL', 'https://1389287830635.firebaseio.com/')

;

