angular.module('myrecipes.services', [
	// 'ngStorage',
  'ui.router',
  'firebase'
])



// a simple utility to create references to Firebase paths
.factory('firebaseRef', ['Firebase', 'FBURL', function(Firebase, FBURL) {
   /**
    * @function
    * @name firebaseRef
    * @param {String|Array...} path
    * @return a Firebase instance
    */
   return function(path) {
      return new Firebase(pathRef([FBURL].concat(Array.prototype.slice.call(arguments))));
   };
}])


 .factory('loginService', ['$rootScope', '$firebaseAuth', 'firebaseRef', '$firebase', '$timeout', function($rootScope, $firebaseAuth, firebaseRef, $firebase, $timeout) {
  var auth = null;
  return {
     init: function(path) {
        return auth = $firebaseAuth(firebaseRef(), {path: path});
     },

     /**
      * @param {string} email
      * @param {string} pass
      * @param {Function} [callback]
      * @returns {*}
      */
     login: function(provider, callback) {
        assertAuth();
        auth.$login(provider).then(function(user) {
        $rootScope.$on("$firebaseAuth:login", function(e, user) {
          var userref = $firebase(firebaseRef(['users',user.id,'info']));
          console.log(user);
          userref.$set({'name':user.displayName, 'username':user.username, 'first':user.first_name, 'last':user.last_name});
        });
          // userref.$add({name: user.});
           if( callback ) {
              //todo-bug https://github.com/firebase/angularFire/issues/199
              $timeout(function() {
                 callback(null, user);
              });
           }
        }, callback);
     },
     isLoggedIn: function(){ 
      // return $localStorage.LOGGEDIN == 'yes'; 
      return !!($rootScope.auth.user);
      // return true;
      },
     logout: function() {
        assertAuth();
        auth.$logout();
     }
  };

  function assertAuth() {
     if( auth === null ) { throw new Error('Must call loginService.init() before using its methods'); }
  }
}])

// .factory('authService', function($rootScope, $localStorage, $state){
//   // $rootScope.userid = null;
//   // $rootScope.$storage = $localStorage;
//   return {

//     isLoggedIn: function(){ 
//       // return $localStorage.LOGGEDIN == 'yes'; 
//       return !!($rootScope.auth.user);
//       // return true;
//     },
//     // logIn: function(user,pass){  
//     //   if( user=='admin' && pass=='123' ){
//     //     $localStorage.LOGGEDIN= 'yes';
//     //     $state.go('main.listing'); 
//     //   }
//     // },
//     logIn: function(){
//       $rootScope.auth.$login('facebook').then(function(user) {
//         $localStorage.LOGGEDIN= 'yes';
//          // $rootScope.userid = user.id;
//          $state.go('main.listing'); 
//       }, function(error) {
//          console.error('Login failed: ', error);
//       });
//     },
//     logOut: function(){ 
//       $localStorage.LOGGEDIN = 'no'; 
//       $rootScope.auth.$logout();
//     }
//   };
// })

// .factory('recipeService', function($q, $timeout, $localStorage, $rootScope, firebaseRef){
  
//   // var items = $firebase(firebaseRef(['users',$rootScope.auth.user.id,'recipe']));
  


//   var activeRecipe,
//       recipes =[];

//   return {
//     loadAllRecipes: function(){
//       // return recipeslist;
//         var deferred = $q.defer(),
//           self = this;

//         //do some async shit here
//         $timeout(function(){
//             // console.log("here");
            
//             deferred.resolve(self.getRecipes); 
//         },500);
//         return deferred.promise;
//     },
//     getRecipes: function() {
//       return $localStorage.recipeslist;
//     },
//     addRecipe: function(item){
      
//       $localStorage.recipeslist.push(item);
//     }
//   };
// })

;


function errMsg(err) {
      return err? typeof(err) === 'object'? '['+err.code+'] ' + err.toString() : err+'' : null;
}

function pathRef(args) {
  for(var i=0; i < args.length; i++) {
     if( typeof(args[i]) === 'object' ) {
        args[i] = pathRef(args[i]);
     }
  }
  return args.join('/');
}