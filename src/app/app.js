angular.module( 'myrecipes', [
  'templates-app',
  'templates-common',
  'myrecipes.login',
  'myrecipes.main',
  'myrecipes.cooks',
  'myrecipes.services',
  'ui.router',
  'ui.route',
  'ngStorage',
  'firebase',
  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'ui.bootstrap.collapse'
  
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/login' );
})



.run(['loginService','$rootScope','$location','$localStorage','$firebaseAuth', function (loginService, $rootScope, $location, $localStorage,$firebaseAuth) {

  // var ref = new Firebase(FBURL);
  // $rootScope.auth = $firebaseAuth(ref);

  // $localStorage.$default({
  //   LOGGEDIN: 'no'
  // });
  
  $localStorage.recipeslist = $localStorage.recipeslist || [
    // {id:'1',name: 'recipe 1', ingredients: 'list of ingredients', instructions: 'put stuff together'},
    // {id:'2',name: 'recipe 2', ingredients: 'another list of ingredients', instructions: 'put stuff in microwave'}
  ];

  $localStorage.nextID =  $localStorage.nextID || 0;
  
  $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
    // if route requires auth and user is not logged in
    // console.log("current user ", $rootScope.auth.user);
    if ( !($rootScope.auth.user) ) {
      // redirect back to login
      $location.path('/login');
    }
  });

    $rootScope.$on("$firebaseAuth:login", function(e, user) {
      console.log("User " + user.id + " successfully logged in!");
      $rootScope.userid = user.id;
    });
    $rootScope.$on("$firebaseAuth:logout", function() {
      console.log("User logged out.");
    });
    $rootScope.$on("$firebaseAuth:error", function(e) {
      console.log("Error: " + e);
    });        

}])


.controller( 'AppCtrl', function AppCtrl (loginService, $scope, $location, $localStorage) {
 
  $scope.logOut = loginService.logout;
  $scope.isLoggedIn = loginService.isLoggedIn;
  
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | myrecipes' ;
    }
  });

})

.constant('FBURL', 'https://1389287830635.firebaseio.com/')

;

