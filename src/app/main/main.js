/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'myrecipes.main', [
  'ui.router',
  'myrecipes.services',
  'ngStorage',
  'firebase',
  'lineBreaks'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider
  .state( 'main', {
    abstract: true,
    url: '/main',
    views: {
      "main": {
        controller: 'MainCtrl',
        templateUrl: 'main/main.tpl.html'
      }
    },
    
    data:{ pageTitle: 'Recipes' }
  })

  .state('main.listing', {
    url: '/listing',
    parent: 'main',
    views:{
      "content": {
        controller: 'FeedCtrl',
        templateUrl: "main/mainFeed.tpl.html"
      }
    }
  })

  .state('main.recipe', {
    url: '/recipe/:id',
    parent: 'main',
    views:{
      "content": {
        controller: 'ViewCtrl',
        templateUrl: 'main/recipeView.tpl.html'
      }
    }
  })

  .state('main.edit', {
    url: '/edit/:id',
    parent: 'main',
    views:{
      "content": {
        controller: 'EditCtrl',
        templateUrl: 'main/recipeEdit.tpl.html'
      }
    }
  })

  .state('main.newrecipe', {
    url: '/newrecipe',
    parent: 'main',
    views:{
      "content": {
        controller: 'NewCtrl',
        templateUrl: 'main/recipeEdit.tpl.html'
      }
    }
  })
  ;

})

.run(['$rootScope', '$state', function ( $rootScope, $state) {
  if ( !($rootScope.auth.user) ) {
      // redirect back to login
      $state.go('login');
  }
}])


/**
 * And of course we define a controller for our route.
 */
.controller( 'MainCtrl', function MainController($rootScope, $scope, $state, $stateParams, $firebase, firebaseRef ) {
   
  $scope.mainref = $firebase(firebaseRef());
  $scope.items = $scope.mainref.$child('users').$child($rootScope.auth.user.id).$child('recipe');
  $scope.userlist = $scope.mainref.$child('users');
  $scope.fullfeed = $scope.mainref.$child('mainfeed');
  // console.log($scope.mainref.$child('users'));

  // $scope.items.$add({foo: "bar"});
  // $scope.recipes = recipes();
  // $scope.$storage = $localStorage;

  // if($scope.recipes.length>0){
  //   for (var i = 0, len = $scope.recipes.length; i < len; i++) {
  //         $scope.lookup[$scope.recipes[i].id] = $scope.recipes[i];
  //   }
  // }
  // $rootScope.$on('newRecipeEmit',function(event){
  //   console.log("got emit");
  //   $scope.recipes = recipes();
  //   if($scope.recipes.length>0){
  //   for (var i = 0, len = $scope.recipes.length; i < len; i++) {
  //         $scope.lookup[$scope.recipes[i].id] = $scope.recipes[i];
  //   }
  // }
  // });
  
  $scope.checkActive = function(recid){
    
    // console.log($state);
    if(( $state.includes('main.recipe') || $state.includes('main.edit') ) && $state.params.id == recid){
      return true;
    }
    else{
      return false;
    }
   
  };
})

.filter('formatText', function (){
  return function(input) {
    if(!input){ return input; }
    var output = input
      //replace possible line breaks.
      .replace(/(\r\n|\r|\n)/g, '<br/>')
      //replace tabs
      .replace(/\t/g, '&nbsp;&nbsp;&nbsp;')
      //replace spaces.
      .replace(/ /g, '&nbsp;');

      return output;
  };
})

.controller( 'FeedCtrl', ['$rootScope', '$scope', '$location', '$stateParams', '$firebase', 'firebaseRef', function FeedCtrl($rootScope, $scope, $location, $stateParams, $firebase, firebaseRef ) {
    
}])

.controller( 'ViewCtrl', ['$rootScope', '$scope', '$location', '$stateParams', '$firebase', 'firebaseRef', function ViewCtrl($rootScope, $scope, $location, $stateParams, $firebase, firebaseRef ) {
    
    $scope.currid = $stateParams.id;
    $scope.item = $scope.items.$child($scope.currid);

    // console.log($scope.item);
    
    // var rid = parseInt($stateParams.id,10);
    // console.log("rid "+rid);
    // console.log(typeof(rid));
    // console.log("recipes ",$scope.recipes);
    // var index = _.findIndex($scope.recipes, {'id':rid});
    // console.log("index ",index);
    // $scope.recipe = $scope.recipes[index];
    // console.log($scope.recipe);
    // $scope.recipe = $scope.lookup[id];
    
  // console.log($scope.recipe);
}])

.controller( 'EditCtrl', ['$rootScope','$scope', '$location', '$stateParams', '$localStorage',  function ViewCtrl($rootScope, $scope, $location, $stateParams, $localStorage ) {

  $scope.currid = $stateParams.id;
  $scope.item = $scope.items.$child($scope.currid);


  // $scope.rid = parseInt($stateParams.id,10);
  // var index = _.findIndex($scope.recipes, {'id':$scope.rid});
  // $scope.recipe = $scope.recipes[index];

  $scope.save = function(){
      
      $scope.item.$set({'name': $scope.item.name, 'ingredients': $scope.item.ingredients, 'instructions': $scope.item.instructions, 'lastedit':Firebase.ServerValue.TIMESTAMP});

      $scope.fullfeed.$child($scope.currid).$set({'name': $scope.item.name, 'poster':$rootScope.auth.user.id});

      $location.path('main/recipe/'+$stateParams.id);
      // old localstorage save method
      // for (var i in $localStorage.recipeslist) {
      //    if ($localStorage.recipeslist[i].id == $scope.rid) {
      //       $localStorage.recipeslist[i] = {'id':$scope.rid,'name': $scope.recipe.name, 'ingredients': $scope.recipe.ingredients, 'instructions': $scope.recipe.instructions};
      //       
      //    }
      //  }
      // $localStorage.recipeslist.push({id:'3',name: 'recipe 3', ingredients: 'another list of ingredients', instructions: 'put stuff in microwave'});
  };

  $scope.delRec = function(){
    $scope.mainref.$child('mainfeed').$child($scope.currid).$remove();
    $scope.item.$remove();
    
    // var removed = _.remove($scope.recipes, {'id':$scope.rid});
    $location.path('main/listing');

  };

}])

.controller( 'NewCtrl', ['$rootScope','$scope', '$localStorage','$location', '$firebase','firebaseRef',function NewCtrl( $rootScope, $scope, $localStorage, $location, $firebase, firebaseRef ) {

  $scope._ref = $firebase(firebaseRef());  
  $scope.items = $scope._ref.$child('users').$child($rootScope.auth.user.id).$child('recipe');
  $scope.recipe = {};

  $scope.save = function(){
 
      // console.log($scope.recipe);
      // var rid = $localStorage.nextID++;
      // console.log(typeof(rid));
      // console.log($scope.items);
      
      var newrec = {'name': $scope.item.name, 'ingredients': $scope.item.ingredients, 'instructions': $scope.item.instructions, 'lastedit':Firebase.ServerValue.TIMESTAMP};

      var newRecipe = $scope.items.$add( newrec );


      $scope._ref.$child('mainfeed').$child(newRecipe.name()).$set({'name': $scope.item.name, 'poster':$rootScope.auth.user.id});


      $location.path('main/recipe/'+ newRecipe.name());
        
      // $scope.items.$child("users").$child($rootScope.userid).$child("recipe").$add(newrec);
      
      // recipeService.addRecipe({'id':rid,'name': $scope.recipe.name, 'ingredients': $scope.recipe.ingredients, 'instructions': $scope.recipe.instructions});
      // $location.path('main/recipe/'+ newRecipe);
      // $scope.$emit('newRecipeEmit');
  };

}])

// .controller( 'ListCtrl', ['$scope', 'recipes', '$stateParams', function ListCtrl( $scope, recipes, $stateParams ){
//   $scope.recipes = recipes;
//   // $scope.id = $stateParams.id;
//   // console.log(recipes);
// }])
;

